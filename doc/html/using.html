

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <title>The YACS User Guide &mdash; yacs 6.5.0 documentation</title>
    
    <link rel="stylesheet" href="_static/default.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    '',
        VERSION:     '6.5.0',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <link rel="top" title="yacs 6.5.0 documentation" href="index.html" />
    <link rel="next" title="Reminders about the SALOME platform" href="rappels.html" />
    <link rel="prev" title="YACS documentation" href="index.html" /> 
  </head>
  <body>
    <div class="related">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="rappels.html" title="Reminders about the SALOME platform"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="index.html" title="YACS documentation"
             accesskey="P">previous</a> |</li>
        <li><a href="index.html">yacs 6.5.0 documentation</a> &raquo;</li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body">
            
  <div class="section" id="the-yacs-user-guide">
<span id="using-index"></span><h1>The YACS User Guide<a class="headerlink" href="#the-yacs-user-guide" title="Permalink to this headline">¶</a></h1>
<p>There is an increasing need for multidisciplinary parametric simulations in various research and engineering fields.
Examples are fluid-structure interaction and thermal coupling.
The simulation tools have become very sophisticated in their own domains, so multidisciplinary simulation can be achieved
by coupling the existing codes.</p>
<p>YACS is a tool for managing multidisciplinary simulations through calculation schemes.</p>
<p>The YACS module can be used to build and execute calculation schemes.  A calculation scheme is a more or less complex
assembly of calculation components (SALOME components or calculation codes or python scripts).
Therefore, a calculation scheme provides a means of defining a chain or coupling of calculation codes (see <a class="reference internal" href="principes.html#principes"><em>YACS general principles</em></a>).</p>
<img alt="_images/ihm.png" class="align-center" src="_images/ihm.png" />
<p class="centered">
<strong>YACS GUI</strong></p><p>A calculation scheme can be built either using a graphic tool (see <a class="reference internal" href="gui.html#gui"><em>Using YACS with the graphical user interface (GUI)</em></a>),
or by editing an XML file directly (see <a class="reference internal" href="schemaxml.html#schemaxml"><em>Defining a calculation scheme in the XML format</em></a>), or by using
an application programming interface (API) in Python (see <a class="reference internal" href="schemapy.html#schemapy"><em>Defining a calculation scheme with the Python programming interface</em></a>).
In this phase, chaining of components is defined with the associated dataflows.</p>
<p>The calculation scheme can be executed from the graphic tool (see <a class="reference internal" href="execution.html#execution"><em>Execution of a schema</em></a>), but also in console
mode (see <a class="reference internal" href="execxml.html#execxml"><em>Executing a calculation scheme in console mode</em></a>), or by using the Python interface (see <a class="reference internal" href="execpy.html#execpy"><em>Execution of a calculation scheme with the Python programming interface</em></a>).</p>
<p>Executing a calculation scheme includes:</p>
<blockquote>
<div><ul class="simple">
<li>running and distributing components</li>
<li>managing data distribution</li>
<li>monitoring execution</li>
<li>stopping / suspending / restarting the execution</li>
</ul>
</div></blockquote>
<p>A calculation scheme can also be executed using a batch system such as LSF or PBS (see <a class="reference internal" href="batch.html#batch"><em>Starting a SALOME application in a batch manager</em></a>).</p>
<p>Finally, it is essential that calculation codes should be transformed into SALOME components, so that they can
be coupled with YACS. In general, this operation requires good knowledge of SALOME principles (see <a class="reference internal" href="integration.html#integration"><em>Integration of components into SALOME for YACS</em></a>).</p>
<p>For C++ calculation codes, the <a class="reference internal" href="hxx2salome.html#hxx2salome"><em>hxx2salome : a SALOME component generator</em></a> tool automates this operation to a large extent.</p>
<p>For Fortran, C and Python calculation codes that use CALCIUM type coupling, the <a class="reference internal" href="yacsgen.html#yacsgen"><em>YACSGEN: SALOME module generator</em></a> tool automatically
generates the necessary SALOME embedment starting from a brief description of the selected coupling interface.</p>
<div class="toctree-wrapper compound">
<ul>
<li class="toctree-l1"><a class="reference internal" href="rappels.html">Reminders about the SALOME platform</a><ul>
<li class="toctree-l2"><a class="reference internal" href="rappels.html#definitions">Definitions</a></li>
<li class="toctree-l2"><a class="reference internal" href="rappels.html#salome-modules-and-components">SALOME modules and components</a></li>
<li class="toctree-l2"><a class="reference internal" href="rappels.html#containers">Containers</a></li>
<li class="toctree-l2"><a class="reference internal" href="rappels.html#construction-and-use-of-a-salome-application">Construction and use of a SALOME application</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="principes.html">YACS general principles</a><ul>
<li class="toctree-l2"><a class="reference internal" href="principes.html#data-types">Data types</a></li>
<li class="toctree-l2"><a class="reference internal" href="principes.html#ports">Ports</a></li>
<li class="toctree-l2"><a class="reference internal" href="principes.html#elementary-calculation-nodes">Elementary calculation nodes</a></li>
<li class="toctree-l2"><a class="reference internal" href="principes.html#connections">Connections</a></li>
<li class="toctree-l2"><a class="reference internal" href="principes.html#composite-nodes">Composite nodes</a></li>
<li class="toctree-l2"><a class="reference internal" href="principes.html#containers">Containers</a></li>
<li class="toctree-l2"><a class="reference internal" href="principes.html#states-of-a-node">States of a node</a></li>
<li class="toctree-l2"><a class="reference internal" href="principes.html#context-sensitive-naming-of-nodes">Context sensitive naming of nodes</a></li>
<li class="toctree-l2"><a class="reference internal" href="principes.html#active-study">Active study</a></li>
<li class="toctree-l2"><a class="reference internal" href="principes.html#error-report">Error report</a></li>
<li class="toctree-l2"><a class="reference internal" href="principes.html#execution-trace-files">Execution trace files</a></li>
<li class="toctree-l2"><a class="reference internal" href="principes.html#execution-of-concurrent-branches">Execution of concurrent branches</a></li>
<li class="toctree-l2"><a class="reference internal" href="principes.html#schema-shutdown">Schema shutdown</a></li>
<li class="toctree-l2"><a class="reference internal" href="principes.html#yacs-general-architecture">YACS general architecture</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="gui.html">Using YACS with the graphical user interface (GUI)</a><ul>
<li class="toctree-l2"><a class="reference internal" href="gui.html#yacs-gui-description">YACS GUI description</a></li>
<li class="toctree-l2"><a class="reference internal" href="gui.html#functionality-list">Functionality list</a></li>
<li class="toctree-l2"><a class="reference internal" href="gui.html#yacs-schema-examples">YACS schema examples</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="console.html">Using YACS in console mode</a><ul>
<li class="toctree-l2"><a class="reference internal" href="schemaxml.html">Defining a calculation scheme in the XML format</a></li>
<li class="toctree-l2"><a class="reference internal" href="execxml.html">Executing a calculation scheme in console mode</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="python.html">Using YACS with the python interface (TUI)</a><ul>
<li class="toctree-l2"><a class="reference internal" href="schemapy.html">Defining a calculation scheme with the Python programming interface</a></li>
<li class="toctree-l2"><a class="reference internal" href="execpy.html">Execution of a calculation scheme with the Python programming interface</a></li>
<li class="toctree-l2"><a class="reference internal" href="advancepy.html">Advanced use of the Python programming interface</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="batch.html">Starting a SALOME application in a batch manager</a><ul>
<li class="toctree-l2"><a class="reference internal" href="batch.html#principles">Principles</a></li>
<li class="toctree-l2"><a class="reference internal" href="batch.html#description-of-the-cluster-using-the-catalogresource-xml-file">Description of the cluster using the CatalogResource.xml file</a></li>
<li class="toctree-l2"><a class="reference internal" href="batch.html#using-the-launcher-service">Using the Launcher service</a></li>
<li class="toctree-l2"><a class="reference internal" href="batch.html#salome-on-the-batch-cluster">SALOME on the batch cluster</a></li>
<li class="toctree-l2"><a class="reference internal" href="batch.html#salome-constraints-on-batch-managers">SALOME constraints on batch managers</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="optimizer.html">Definition of an algorithm for the OptimizerLoop</a><ul>
<li class="toctree-l2"><a class="reference internal" href="optimizer.html#synchronous-algorithm">Synchronous algorithm</a></li>
<li class="toctree-l2"><a class="reference internal" href="optimizer.html#asynchronous-algorithm">Asynchronous algorithm</a></li>
<li class="toctree-l2"><a class="reference internal" href="optimizer.html#c-algorithm-calling-python-code">C++ algorithm calling Python code</a></li>
</ul>
</li>
</ul>
</div>
</div>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar">
        <div class="sphinxsidebarwrapper">
  <h4>Previous topic</h4>
  <p class="topless"><a href="index.html"
                        title="previous chapter">YACS documentation</a></p>
  <h4>Next topic</h4>
  <p class="topless"><a href="rappels.html"
                        title="next chapter">Reminders about the SALOME platform</a></p>
  <h3>This Page</h3>
  <ul class="this-page-menu">
    <li><a href="_sources/using.txt"
           rel="nofollow">Show Source</a></li>
  </ul>
<div id="searchbox" style="display: none">
  <h3>Quick search</h3>
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" />
      <input type="submit" value="Go" />
      <input type="hidden" name="check_keywords" value="yes" />
      <input type="hidden" name="area" value="default" />
    </form>
    <p class="searchtip" style="font-size: 90%">
    Enter search terms or a module, class or function name.
    </p>
</div>
<script type="text/javascript">$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="rappels.html" title="Reminders about the SALOME platform"
             >next</a> |</li>
        <li class="right" >
          <a href="index.html" title="YACS documentation"
             >previous</a> |</li>
        <li><a href="index.html">yacs 6.5.0 documentation</a> &raquo;</li> 
      </ul>
    </div>
    <div class="footer">
        &copy; Copyright 2007-2011 CEA/DEN, EDF R&amp;D, OPEN CASCADE, C. Caremoli, N. Crouzet, P. Rascle, A. Ribes, E. Fayolle, M. Tajchman.
      Created using <a href="http://sphinx.pocoo.org/">Sphinx</a> 1.1.3.
    </div>
  </body>
</html>